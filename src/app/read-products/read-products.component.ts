import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProductsService } from '../products.service';
import { Observable } from 'rxjs';
import { Product } from '../product';


@Component({
  selector: 'app-read-products',
  templateUrl: './read-products.component.html',
  styleUrls: ['./read-products.component.css'],
  providers: [ProductsService]
})
export class ReadProductsComponent implements OnInit {
  products: Product[];
  @Output() show_create_product_event= new EventEmitter();
  @Output() show_update_product_event=new EventEmitter();
  @Output() show_delete_product_event=new EventEmitter();
  @Output() show_read_one_product_event=new EventEmitter();
  constructor(private productService: ProductsService) { }

  createProduct(){
    this.show_create_product_event.emit({
      title: "Create Product"
    });
  }
  readOneProduct(id){
     console.log('rp comp readOneProduct');
        // tell the parent component (AppComponent)
        this.show_read_one_product_event.emit({
            product_id: id,
            title: "Read One Product"
    });
  }
  updateProduct(id){}
  deleteProduct(id){
    // tell the parent component (AppComponent)
        this.show_delete_product_event.emit({
       product_id: id,
              title: "Delete Product"
           });
  }

  ngOnInit() {
    this.productService.readProducts().subscribe(products => 
                this.products=products['records']);
  }

}
