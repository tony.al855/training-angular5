import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';
import { catchError } from 'rxjs/operators';
import { Category } from './category';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(private _http: Http) { }

  readCategories(): Observable<Category[]>{
            return this._http
                .get("http://localhost/api/category/read.php")
                .pipe(map((res: Response) => res.json()));
        }
     
}
